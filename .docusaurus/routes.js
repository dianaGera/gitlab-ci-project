import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/gitlab-ci-project/__docusaurus/debug',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug', '63d'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/__docusaurus/debug/config',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug/config', '232'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/__docusaurus/debug/content',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug/content', 'b5b'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/__docusaurus/debug/globalData',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug/globalData', '48f'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/__docusaurus/debug/metadata',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug/metadata', 'a9f'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/__docusaurus/debug/registry',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug/registry', '45c'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/__docusaurus/debug/routes',
    component: ComponentCreator('/gitlab-ci-project/__docusaurus/debug/routes', '9ce'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog',
    component: ComponentCreator('/gitlab-ci-project/blog', 'bb2'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/archive',
    component: ComponentCreator('/gitlab-ci-project/blog/archive', 'f5a'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/first-blog-post',
    component: ComponentCreator('/gitlab-ci-project/blog/first-blog-post', '664'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/long-blog-post',
    component: ComponentCreator('/gitlab-ci-project/blog/long-blog-post', '6b4'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/mdx-blog-post',
    component: ComponentCreator('/gitlab-ci-project/blog/mdx-blog-post', 'f2c'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/tags',
    component: ComponentCreator('/gitlab-ci-project/blog/tags', 'acc'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/tags/docusaurus',
    component: ComponentCreator('/gitlab-ci-project/blog/tags/docusaurus', 'cbb'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/tags/facebook',
    component: ComponentCreator('/gitlab-ci-project/blog/tags/facebook', '969'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/tags/hello',
    component: ComponentCreator('/gitlab-ci-project/blog/tags/hello', 'fad'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/tags/hola',
    component: ComponentCreator('/gitlab-ci-project/blog/tags/hola', '2dd'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/blog/welcome',
    component: ComponentCreator('/gitlab-ci-project/blog/welcome', 'c7c'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/markdown-page',
    component: ComponentCreator('/gitlab-ci-project/markdown-page', 'cda'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/my-markdown-page',
    component: ComponentCreator('/gitlab-ci-project/my-markdown-page', '6a0'),
    exact: true
  },
  {
    path: '/gitlab-ci-project/docs',
    component: ComponentCreator('/gitlab-ci-project/docs', '38d'),
    routes: [
      {
        path: '/gitlab-ci-project/docs',
        component: ComponentCreator('/gitlab-ci-project/docs', '9bf'),
        routes: [
          {
            path: '/gitlab-ci-project/docs',
            component: ComponentCreator('/gitlab-ci-project/docs', '5ec'),
            routes: [
              {
                path: '/gitlab-ci-project/docs/category/tutorial---basics',
                component: ComponentCreator('/gitlab-ci-project/docs/category/tutorial---basics', 'aec'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/category/tutorial---extras',
                component: ComponentCreator('/gitlab-ci-project/docs/category/tutorial---extras', '2c4'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/intro',
                component: ComponentCreator('/gitlab-ci-project/docs/intro', '794'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-basics/congratulations',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-basics/congratulations', '645'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-basics/create-a-blog-post',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-basics/create-a-blog-post', '00f'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-basics/create-a-document',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-basics/create-a-document', 'be9'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-basics/create-a-page',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-basics/create-a-page', '3fb'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-basics/deploy-your-site',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-basics/deploy-your-site', '610'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-basics/markdown-features',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-basics/markdown-features', 'd0f'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-extras/manage-docs-versions',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-extras/manage-docs-versions', 'acc'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/gitlab-ci-project/docs/tutorial-extras/translate-your-site',
                component: ComponentCreator('/gitlab-ci-project/docs/tutorial-extras/translate-your-site', 'f87'),
                exact: true,
                sidebar: "tutorialSidebar"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/gitlab-ci-project/',
    component: ComponentCreator('/gitlab-ci-project/', 'f2f'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
